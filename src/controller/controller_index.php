<?php

function actionIndex($twig, $db) {
    $form = array();

    echo $twig->render('index.html.twig', array('form' => $form));
}

function actionMentions() {
    header("Location: ../web/other/mentions.pdf");
}

function actionCgu($twig, $db) {
    $form = array();

    echo $twig->render('cgu.html.twig', array('form' => $form));
}

function actionContact($twig, $db) {
    $form = array();

    if (isset($_POST['btSendForm'])) {
        $email = htmlspecialchars($_POST['email']);
        $reason = htmlspecialchars($_POST['reason']);

        if (isset($email) && !empty($email) && isset($reason) && !empty($reason)) {
            //Envoie d'un mail//
            $header = "MIME-Version: 1.0\r\n";
            $header .= "From:'$email'<$email>" . "\n";
            $header .= 'Content-Type:text/html; charset="uft-8"' . "\n";
            $header .= 'Content-Transfer-Encoding: 8bit';

            $message = "
                    <html>
                        <body>
                            <div align='center'>
                                <p>Mail provenant de : $email</p>
                                <p>Soucis: $reason</p>
                            </div>
                        </body>
                    </html>
                   ";

            mail("symfony4.4017@gmail.com", "Un client a besoin d'assistance", $message, $header);
//fin d'envoie du mail//
            $form['valide'] = true;
            $form['message'] = "Le webmaster a bien reçu votre mail, il vous répondra sous peu.";
        } else {
            $form['valide'] = false;
            $form['message'] = "Une erreur s'est produite. Réessayez dans quelques minutes...";
        }
    }

    echo $twig->render('contact.html.twig', array('form' => $form));
}

function actionData() {
    $data = file_get_contents("other/data.json");
    echo $data;
}
