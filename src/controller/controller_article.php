<?php

function actionArticle($twig, $db) {
    $form = array();
    
    $category = new Category($db);
    $article = new Article($db);
    
    $form['category'] = $category->select();
    $form['article'] = $article->select();
    
    echo $twig->render('article.html.twig', array('form' => $form));
}