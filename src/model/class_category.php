<?php

class Category {

    private $db;
    private $insert;
    private $select;
    private $delete;

    public function __construct($db) {
        $this->db = $db;
        $this->insert = $db->prepare("INSERT INTO Category(labelCategory) VALUES(:labelCategory)");
        $this->select = $db->prepare("SELECT idCategory, labelCategory FROM Category");
        $this->delete = $db->prepare("DELETE FROM Category WHERE idCategory=:idCategory");
    }

    public function insert($labelCategory) {
        $r = true;
        $this->insert->execute(array(':labelCategory' => $labelCategory));
        if ($this->insert->errorCode() != 0) {
            print_r($this->insert->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function select() {
        $this->select->execute();
        if ($this->select->errorCode() != 0) {
            print_r($this->select->errorInfo());
        }
        return $this->select->fetchAll();
    }

    public function delete($idCategory) {
        $r = true;
        $this->delete->execute(array(':idCategory' => $idCategory));
        if ($this->delete->errorCode() != 0) {
            print_r($this->delete->errorInfo());
            $r = false;
        }
        return $r;
    }
}
