<?php

class Article {

    private $db;
    private $insert;
    private $select;
    private $update;
    private $delete;

    public function __construct($db) {
        $this->db = $db;
        $this->insert = $db->prepare("INSERT INTO Article(titleArticle, contentArticle, dateArticle, sourceArticle, imageArticle, idCategory, idUser) VALUES(:titleArticle, :contentArticle, :dateArticle, :sourceArticle, :imageArticle, :idCategory, :idUser)");
        $this->select = $db->prepare("SELECT titleArticle, contentArticle, dateArticle, sourceArticle, imageArticle, c.labelCategory, u.nicknameUser 
                                      FROM Article a 
                                      INNER JOIN Category c ON c.idCategory=a.idCategory 
                                      INNER JOIN User u ON u.idUser=a.idUser 
                                      ORDER BY dateArticle ");
        $this->update = $db->prepare("UPDATE Article SET titleArticle=:titleArticle, contentArticle=:contentArticle, sourceArticle=:sourceArticle, imageArticle=:imageArticle, idCategory=:idCategory, idUser=:idUser 
                                      WHERE idArticle=:idArticle");
        $this->delete = $db->prepare("DELETE FROM Article WHERE idArticle=:idArticle");
    }

    public function insert($titleArticle, $contentArticle, $dateArticle, $sourceArticle, $imageArticle, $idCategory, $idUser) {
        $r = true;
        $this->insert->execute(array(':titleArticle' => $titleArticle, ':contentArticle' => $contentArticle, ':dateArticle' => $dateArticle, ':sourceArticle' => $sourceArticle, ':imageArticle' => $imageArticle, ':idCategory' => $idCategory, ':idUser' => $idUser));
        if ($this->insert->errorCode() != 0) {
            print_r($this->insert->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function select() {
        $this->select->execute();
        if ($this->select->errorCode() != 0) {
            print_r($this->select->errorInfo());
        }
        return $this->select->fetchAll();
    }

    public function update($titleArticle, $contentArticle, $dateArticle, $sourceArticle, $imageArticle, $idCategory, $idUser) {
        $r = true;
        $this->update->execute(array(':titleArticle' => $titleArticle, ':contentArticle' => $contentArticle, ':dateArticle' => $dateArticle, ':sourceArticle' => $sourceArticle, ':imageArticle' => $imageArticle, ':idCategory' => $idCategory, ':idUser' => $idUser));
        if ($this->update->errorCode() != 0) {
            print_r($this->update->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function delete($idArticle) {
        $r = true;
        $this->delete->execute(array(':idArticle' => $idArticle));
        if ($this->delete->errorCode() != 0) {
            print_r($this->delete->errorInfo());
            $r = false;
        }
        return $r;
    }
}
