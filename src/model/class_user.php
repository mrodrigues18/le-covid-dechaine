<?php

class User {

    private $db;
    private $insert;
    private $connect;
    private $select;
    private $selectByNickname;
    private $updateMdp;
    private $delete;
    private $search;

    public function __construct($db) {
        $this->db = $db;
        $this->insert = $db->prepare("INSERT INTO User(nicknameUser, passwordUser, idRole) VALUES(:nicknameUser, :passwordUser, :idRole)");
        $this->connect = $db->prepare("SELECT idUser, nicknameUser, idRole, passwordUser FROM User WHERE nicknameUser=:nicknameUser");
        $this->select = $db->prepare("SELECT idUser, nicknameUser, labelRole 
                                      FROM User u 
                                      INNER JOIN Role r ON r.idRole = u.idRole 
                                      ORDER BY nicknameUser");
        $this->selectByNickname = $db->prepare("SELECT idUser, nicknameUser, passwordUser, u.idRole, labelRole
                                                FROM User u
                                                INNER JOIN Role r ON r.idRole=u.idRole 
                                                WHERE nicknameUser=:nicknameUser");
        $this->updateMdp = $db->prepare("UPDATE User SET passwordUser=:passwordUser WHERE nicknameUser=:nicknameUser");
        $this->delete = $db->prepare("DELETE FROM User WHERE nicknameUser=:nicknameUser");
        $this->search = $db->prepare("SELECT titleFaq AS label
                                      FROM Faq
                                      WHERE titleFaq LIKE(CONCAT('%', :saisie, '%'))
                                      UNION 
                                      SELECT labelCategory AS label
                                      FROM Category
                                      WHERE labelCategory LIKE(CONCAT('%', :saisie, '%'))
                                      UNION
                                      SELECT titleArticle AS label
                                      FROM Article
                                      WHERE titleArticle LIKE(CONCAT('%', :saisie, '%'))
                                      ORDER BY label DESC");
    }

    public function insert($nicknameUser, $passwordUser, $idRole) {
        $r = true;
        $this->insert->execute(array(':nicknameUser' => $nicknameUser, ':passwordUser' => $passwordUser, ':idRole' => $idRole));
        if ($this->insert->errorCode() != 0) {
            print_r($this->insert->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function connect($nicknameUser) {
        $aUser = $this->connect->execute(array(':nicknameUser' => $nicknameUser));
        if ($this->connect->errorCode() != 0) {
            print_r($this->connect->errorInfo());
        }
        return $this->connect->fetch();
    }

    public function select() {
        $this->select->execute();
        if ($this->select->errorCode() != 0) {
            print_r($this->select->errorInfo());
        }
        return $this->select->fetchAll();
    }

    public function selectByNickname($nicknameUser) {
        $this->selectByNickname->execute(array(':nicknameUser' => $nicknameUser));
        if ($this->selectByNickname->errorCode() != 0) {
            print_r($this->selectByNickname->errorInfo());
        }
        return $this->selectByNickname->fetch();
    }

    public function updateMdp($nicknameUser, $passwordUser) {
        $r = true;
        $this->updateMdp->execute(array(':nicknameUser' => $nicknameUser, ':passwordUser' => $passwordUser));
        if ($this->updateMdp->errorCode() != 0) {
            print_r($this->updateMdp->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function delete($nicknameUser) {
        $r = true;
        $this->delete->execute(array(':nicknameUser' => $nicknameUser));
        if ($this->delete->errorCode() != 0) {
            print_r($this->delete->errorInfo());
            $r = false;
        }
        return $r;
    }
    
    public function search($saisie) {
        $this->search->execute(array(':saisie' => $saisie));
        if ($this->search->errorCode() != 0) {
            print_r($this->search->errorInfo());
        }
        $response = array();
        while($row = $this->search->fetch()) {
            $response[] = array("label"=>$row['label']);
        }
        echo json_encode($response);
    }
}
?>

